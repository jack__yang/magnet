package org.eocencle.magnet.flink1.component;

import org.eocencle.magnet.core.component.WorkStageResult;

/**
 * Flink作业结果类
 * @author: huan
 * @Date: 2020-05-24
 * @Description:
 */
public class FlinkWorkStageResult extends WorkStageResult {
}
